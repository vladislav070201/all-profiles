const filePickerElement = document.getElementById('image');
const imagePreviewElement = document.getElementById('image-preview');

filePickerElement.addEventListener('change', e => {
  const files = filePickerElement.files;

  if (!files || !files.length) {
    imagePreviewElement.style.display = 'none';
    return;
  }

  imagePreviewElement.src = URL.createObjectURL(files[0]);
  imagePreviewElement.style.display = 'block';
});
